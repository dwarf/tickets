# Installation

You MUST install into webroot.

Get the code:

    $ git clone ... tickets
    $ cd tickets

Initialize db:

    $ mysql -u user -p dbname < schema.sql

Prepare kohana core:

    $ cd application
    $ mkdir cache logs
    $ chmod a+w cache logs

Configuration:

    $ cd ../modules/database/config
    $ cp database.example.php database.php
    $ vi database.php
    $ cd ../recaptcha/config
    $ cp recaptcha.example.php recaptcha.php
    $ vi recaptcha.php

Crontab to auto-close old tickets:

    '@daily /usr/bin/curl http://abuse.rizon.net/home/close_old >/dev/null 2>&1 http://abuse.rizon.net/home/close_old'

