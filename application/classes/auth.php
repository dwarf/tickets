<?php
class Auth {
	private static $i;
	private $user;
	private $userName;
	private $session;
	
	private $modelUser;
	
	protected function __construct() {
		$this->session = Session::instance();
		$this->modelUser = new Model_User();
		$u = $this->session->get('user', null);
		if ($u != null) {
			$this->user = $u;
			//die(var_dump($u));

			$data = $this->modelUser->get($u);
			$this->userName = $data['name'];
		}
	}
	
	public static function instance() {
		if (!isset(self::$i)) {
			self::$i = new Auth();
		}
		return self::$i;
	}
	
	public function authenticate($userName, $password) {
		$user = new Model_User();
		
		$isStaff = IN_PRODUCTION?$user->vb3($userName, $password):true;
		if ($isStaff) {
			$data = $this->modelUser->getByName($userName);
			if ($data == null) {
				$x = $this->modelUser->create($userName);
				$this->user = $x[0];
				$this->userName = $userName;
			} else {
				$this->user = $data['user'];
				$this->userName = $data['name'];
			}

			$this->session->set('user', $this->user);
			return true;
		} else {
			return false;
		}
	}
	
	public function clear() {
		$this->session->delete('user');
	}
	
	public function getUser() {
		return $this->user;
	}
	
	public function getUserName() {
		return $this->userName;
	}
}
?>
