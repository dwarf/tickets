<?php 
defined('SYSPATH') or die('No direct script access.');

class Controller_Admin extends Controller_Template {

	public function action_index() {
		$this->page_title = 'Admin';
		$this->auth = Auth::instance();
		
		if ($this->auth->getUser() === null) {
			$c = Request::current();
			$c->redirect('home/login?referrer='.$c->uri());
		}


		$v = View::factory('pages/admin/index');
		$ticket = new Model_Ticket();
		$v->tickets = $ticket->getByAssignee($this->auth->getUser());
		$v->pending = $ticket->getByStatuses(array(0));
		$v->other = $ticket->getByStatuses(array(1,2,3))->as_array();
		foreach ($v->other as $k => $t) {
			if ($t['status'] == 1 && $t['assignee'] == $this->auth->getUser()) {
				unset($v->other[$k]);
			}
		}
		$v->leaderboard = $ticket->leaderboard();
		$this->template->content = $v;
	}

	
} // End Home
