<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Error extends Controller_Template {

	public function action_index() {
		$v = new View('errors/404');
		$this->template->content = $v;
	}

}