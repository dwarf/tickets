<?php 
defined('SYSPATH') or die('No direct script access.');

class Controller_Home extends Controller_Template {
	
	public $template = 'template';
	public function action_index() {
		$this->page_title = 'a';
		$this->template->content = View::factory('pages/home');
	}
	
	public function action_login() {
		$a = Auth::instance();

		$referrer = $this->request->query('referrer');
		if ($referrer && !preg_match('/^[0-9a-z\/]+$/', $referrer)) {
			$referrer = '';
		}
		if ($a->getUser() !== null) {
			if ($referrer) {
				Request::current()->redirect($referrer);
			} else {
				Request::current()->redirect('admin');						
			}
		}

		$v = View::factory('pages/login');
		$input = array(
			'username' => isset($_POST['username'])?$_POST['username']:'',
			'password' => isset($_POST['password'])?$_POST['password']:''
		);
		$v->input = $input;
		$v->referrer = $referrer;
		if (count($_POST) > 0) {
			$validate = new Validate($_POST);
			$validate->rules('username', array('not_empty'=>array(), 'max_length'=>array(255)));
			$validate->rules('password', array('not_empty'=>array()));
			if (!$validate->check()) {
				$v->errors = $validate->errors();
			} else {
				if ($a->authenticate($_POST['username'], $_POST['password'])) {
					if ($referrer) {
						Request::current()->redirect($referrer);					
					} else {
						Request::current()->redirect('admin');						
					}
				}
			}
		}
		$this->template->content = $v;
	}

	public function action_logout() {
		Auth::instance()->clear();
		Request::current()->redirect('');
	}
	
	public function action_close_old() {
		$ticket = new Model_Ticket();
		$ts = $ticket->closeOld(259200);
		foreach ($ts as $t) {
			$mail = new Mail($t['contact_name'], $t['contact_email']);
			$mail->ticket_autoclosed($t['ticket']);
		}
		$this->auto_render = false;
	}
} // End Home
