<?php 
defined('SYSPATH') or die('No direct script access.');

class Controller_Search extends Controller_Template {
	
	public $template = 'template';
	public function action_index() {
		$this->page_title = 'Search';
		$view = View::factory('pages/search');
		if ($this->request->method() !== 'POST'
			|| !($term = $this->request->post('term'))) {
			$this->template->content = $view;
			return;
		}

		$ticket = new Model_Ticket();

		$view->last_term = HTML::chars($term);
		if (substr($term, 0, 1) === '&') {
			$term = substr($term, 1);
			$view->results = $ticket->getBySearchExact($term);
		} else {
			$view->results = $ticket->getBySearch($term);
		}
		$this->template->content = $view;
	}
	
} // End Search
