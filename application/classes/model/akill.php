<?php 
class Model_Akill extends Model {

	public function add(array $as) {
		$q = DB::insert('akill', array('ip', 'time', 'message', 'addedby', 'timestamp', 'akillid'))->values($as);
		return $q->execute();
	}
	
	public function get($akill) {
		$q = DB::query(Database::SELECT, 'SELECT akill, ip, time, message, addedby, timestamp, akillid FROM akill WHERE akill = :akill LIMIT 1')->param(':akill', $akill);
		$r = $q->execute();
		if (count($r) > 0) {
			return $r[0];
		}
		return false;
	}

	public function getByIp($ip) {
		$q = DB::query(Database::SELECT, 'SELECT akill, ip, time, message, addedby, timestamp, akillid FROM akill WHERE ip = :ip LIMIT 1')->param(':ip', '*@'.$ip);
		return $q->execute();

	}

	public function getGeo($ip) {
		if (!Kohana::config('database.geo')) {
			return false;
		}
		$q = DB::query(Database::SELECT, 'SELECT ip, time, message, addedby, FROM_UNIXTIME(timestamp) AS timestamp, akillid FROM akill WHERE ip = :ip LIMIT 1')->param(':ip', $ip);
		$r = $q->execute('geo');
		if (count($r) > 0) {
			return $r[0];
		}
		return false;
	}
}
