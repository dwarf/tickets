<?php 
class Model_Banned extends Model {

    public function check($ip) {
        $q = DB::query(Database::SELECT, 'SELECT 1 FROM banned WHERE ip = :ip LIMIT 1')->param(':ip', $ip);
        $r = $q->execute();
        if (count($r) > 0) {
            return true;
        }
        return false;
    }

}
