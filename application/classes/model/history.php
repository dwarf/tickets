<?php 
class Model_History extends Model {

	public function recent() {
		$q = DB::query(Database::SELECT, 'SELECT ticket_history, ticket, message, at, `by`, user.name AS by_name FROM ticket_history LEFT JOIN user ON ticket_history.`by` = user.user WHERE at > NOW() - INTERVAL 30 SECOND ORDER BY at ASC');
		return $q->execute();
	}

}
