<?php 
class Model_Ticket extends Model {

	public function add(array $as) {
		$q = DB::insert('ticket', array('ip', 'real_ip', 'contact_name', 'contact_email', 'ban_message', 'host_number', 'why', 'akill', 'added'))->values(array_merge($as, array('added'=>DB::expr('NOW()'))));
		return $q->execute();
	}
	
	public function getByAssigned($b) {
		$q = DB::query(Database::SELECT, 'SELECT ticket, ip, contact_name, contact_email, ban_message, host_number, why, DATE_FORMAT(added, \'%e %b %T\') AS added, status, assignee, akill, user.name AS assignee_name, last_replier FROM ticket LEFT JOIN user ON user.user = ticket.assignee WHERE assignee IS '.($b ? 'NOT NULL' : 'NULL').' ORDER BY ticket DESC');
		return $q->execute();
	}
	
	public function getByAssignee($i) {
		$q = DB::query(Database::SELECT, 'SELECT ticket, ip, contact_name, contact_email, ban_message, host_number, why, DATE_FORMAT(added, \'%e %b %T\') AS added, status, akill, assignee, last_replier FROM ticket WHERE assignee = :assignee ORDER BY ticket DESC')->param(':assignee', (int) $i);
		return $q->execute();
	}

	public function getByStatuses(array $s) {
		$q = DB::query(Database::SELECT, '
		SELECT ticket, ip, contact_name, contact_email, ban_message, host_number, why, DATE_FORMAT(added, \'%e %b %T\') AS added, status, akill, assignee, user.name AS assignee_name, last_replier
		FROM ticket
		LEFT JOIN user ON user.user = ticket.assignee
		WHERE status IN :statuses
		AND (status != 3 OR last_reply_at > NOW() - INTERVAL 7 DAY)
		ORDER BY ticket DESC')->param(':statuses', $s);
		return $q->execute();
	}

	public function getByIP($ip) {
		$q = DB::query(Database::SELECT, 'SELECT ticket, ip, contact_name, contact_email, ban_message, host_number, why, DATE_FORMAT(added, \'%e %b %T\') AS added, status, akill, assignee, user.name AS assignee_name, last_replier FROM ticket LEFT JOIN user ON user.user = ticket.assignee WHERE ip = :ip ORDER BY ticket DESC')->param(':ip', $ip);
		return $q->execute();
	}

	public function getBySearch($term) {
		$q = DB::query(Database::SELECT, 'SELECT ticket, ip, contact_name, contact_email, ban_message, host_number, why, DATE_FORMAT(added, \'%e %b %T\') AS added, status, akill, assignee, user.name AS assignee_name, last_replier FROM ticket LEFT JOIN user ON user.user = ticket.assignee WHERE ip LIKE :term OR contact_name LIKE :term OR contact_email LIKE :term OR ban_message LIKE :term OR why LIKE :term ORDER BY ticket DESC')->param(':term', "%$term%");
		return $q->execute();
	}

	public function getBySearchExact($term) {
		$q = DB::query(Database::SELECT, 'SELECT ticket, ip, contact_name, contact_email, ban_message, host_number, why, DATE_FORMAT(added, \'%e %b %T\') AS added, status, akill, assignee, user.name AS assignee_name, last_replier FROM ticket LEFT JOIN user ON user.user = ticket.assignee WHERE ip = :term OR contact_name = :term OR contact_email = :term OR ban_message = :term OR why = :term ORDER BY ticket DESC')->param(':term', $term);
		return $q->execute();
	}
	
	public function get($i) {
		$q = DB::query(Database::SELECT, 'SELECT ticket, ip, contact_name, contact_email, ban_message, host_number, why, DATE_FORMAT(added, \'%e %b %Y %T\') AS added, status, akill, assignee, user.name AS assignee_name, last_replier FROM ticket LEFT JOIN user ON user.user = ticket.assignee WHERE ticket = :ticket')->param(':ticket', (int) $i);
		$r = $q->execute();
		if (count($r) > 0) {
			return $r[0];
		}
		return false;
	}
	
	public function assign($t, $u) {
		$q = DB::update('ticket')->where('ticket', '=', $t)->value('assignee', $u===null?null:(int) $u);
		return $q->execute();
	}

	public function replies($t) {
		$q = DB::query(Database::SELECT, 'SELECT ticket_reply, message, `by`, at, user.name AS by_name, ip FROM ticket_reply LEFT JOIN user ON ticket_reply.`by` = user.user WHERE ticket = :ticket')->param(':ticket', (int) $t);
		return $q->execute();
	}

	public function reply($t, $by, $message) {
		if ($by === null) {
			$by_name = 'Ticket Submitter';
		} else {
			$by_name = DB::query(Database::SELECT, 'SELECT user.name AS by_name FROM user WHERE user = :user LIMIT 1')->param(':user', (int) $by)->execute()->get('by_name', 'Ticket Submitter');
		}
		DB::update('ticket')->where('ticket', '=', $t)->value('last_replier', $by_name)->value('last_reply_at', DB::expr('NOW()'))->execute();

		$reply = DB::insert('ticket_reply', array('ticket', 'message', 'at', 'by', 'ip'))->values(array((int) $t, $message, DB::expr('NOW()'), $by, $_SERVER['REMOTE_ADDR']));
		return $reply->execute();
	}
	
	public function history($i) {
		$q = DB::query(Database::SELECT, 'SELECT ticket_history, message, at, `by`, user.name AS by_name FROM ticket_history LEFT JOIN user ON ticket_history.`by` = user.user WHERE ticket = :ticket ORDER BY at DESC')->param(':ticket', (int) $i);
		return $q->execute();
	}
	
	public function addHistory($ticket, $by, $message) {
		return DB::insert('ticket_history', array('ticket', 'message', 'at', 'by'))->values(array((int) $ticket, $message, DB::expr('NOW()'), $by))->execute();
	}
	
	public function setStatus($t, $s) {
		$q = DB::update('ticket')->where('ticket', '=', $t)->value('status', (int) $s);
		return $q->execute();
	}
	
	public function closeOld($seconds) {
		$get = DB::query(Database::SELECT, 'SELECT ticket, contact_name, contact_email FROM ticket WHERE last_reply_at < NOW() - INTERVAL :timediff SECOND AND status = 2')->param(':timediff', (int) $seconds); // grab all resolved tickets older than passed time
		$r = $get->execute();
		$ids = $r->as_array(NULL, 'ticket');

		if (count($ids) > 0) {
			$ids_in = '(' . implode(',', $ids) . ')';
			$close = DB::query(Database::UPDATE, 'UPDATE ticket SET status = 3 WHERE ticket IN '.$ids_in);
			$close->execute();
			foreach ($ids as $t) {
				$this->addHistory($t, null, 'Ticket automatically closed after 72 hours.');
			}
		}
		return $r;
	}
	
	public function leaderboard() {
		// TODO: Optimize (likely just cache)
		$q = DB::query(Database::SELECT, 'SELECT name, COUNT(*) AS c FROM (SELECT ticket.ip, user.name, ticket_reply.by, MAX(ticket_reply.at) FROM ticket LEFT JOIN ticket_reply ON ticket_reply.ticket = ticket.ticket LEFT JOIN user ON user.user = ticket_reply.by WHERE ticket_reply.by IS NOT NULL GROUP BY ticket.ticket) AS t GROUP BY t.by ORDER BY c DESC');
		return $q->execute();
	}

}
