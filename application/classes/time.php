<?php


class Time {
	/**
	 * @param int $delta Delta time in seconds
	 * @return Formated string in terms of relative time.
	 */
	public static function relative($delta) {
		if ($delta < 1 * 60) {
			return $delta == 1 ? "one second" : $delta . " seconds";
		}
		if ($delta < 2 * 60) {
			return "a minute";
		}
		if ($delta < 45 * 60) {
			return floor($delta / 60) . " minutes";
		}
		if ($delta < 90 * 60) {
			return "an hour";
		}
		if ($delta < 24 * 60 * 60) {
			return floor($delta / (60 * 60)) . " hours";
		}
		if ($delta < 30 * 60 * 60 * 24) {
			return floor($delta / (60 * 60 * 24)) . " days";
		}
		if ($delta < 12 * 60 * 60 * 24 * 30) {
			$months = floor($delta / (60 * 60 * 24) / 30);
			return $months <= 1 ? "one month" : $months . " months";
		} else {
			$years = floor($delta / (60 * 60 * 24) / 365);
			return $years <= 1 ? "one year" : $years . " years";
		}
	}
}
?>