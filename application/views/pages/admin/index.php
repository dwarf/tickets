<?php echo View::factory('ticket_list')->bind('tickets', $tickets)->set('ticket_section_title', 'Your tickets')->set('ticket_status_name', 'assigned'); ?>
<?php echo View::factory('ticket_list')->bind('tickets', $pending)->set('ticket_section_title', 'Pending tickets')->set('ticket_status_name', 'pending'); ?>
<?php echo View::factory('ticket_list')->bind('tickets', $other)->set('ticket_section_title', 'Other tickets'); ?>
<h2>Top resolvers</h2>
<table>
	<thead>
		<tr>
			<th>Name</th>
			<th>Count</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($leaderboard as $l) {
			echo '<tr><td>'.HTML::chars($l['name']).'</td><td>'.HTML::chars($l['c']).'</td></tr>';
		}
		?>
	</tbody>
</table>

<script type='text/javascript'>
	var ls = document.getElementsByTagName('a');
	var lsl = ls.length;
	for (var i = 0; i < lsl; i++) {
		var item = ls[i];
		if (item.parentNode.tagName == 'TD'){
			var tr = item.parentNode.parentNode;
			(function(l) {
				tr.onclick = function(e) {
					if (e.ctrlKey || e.shiftKey || e.altKey || e.button != 0) {
						return;
					}
					document.location = l.href;
				};
			})(item);
		}
	}
</script>