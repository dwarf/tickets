<?php 
if ($ticket === false) {
	echo 'No ticket.';
} else {
	
?>
<h2>Ticket #<?php echo HTML::chars($ticket['ticket']); ?></h2>
<table class="info">
	<tr>
		<td class="infoa">IP</td>
		<td><?php
			echo HTML::chars($ticket['ip']);
			if (Auth::instance()->getUser() !== null && count($associatedTickets) > 0) {
				echo ' | Other tickets: <ul>';
				foreach ($associatedTickets as $t) {
					echo '<li><a href="'.URL::site('akills/view/'.$t['ticket']).'">#'.$t['ticket'].'</a> ('.Ticket::status($t['status']).($t['status'] == 1?' by '.HTML::chars($t['assignee_name']):'').') - '.($t['host_number']!='0'?'SLI':'Ban').' - '.$t['added'].'</li>';
				}
				echo '</ul>';
			}
		?></td>
	</tr>
	<tr>
		<td class="infoa">Contact Name</td>
		<td><?php echo HTML::chars($ticket['contact_name']); ?></td>
	</tr>
	<tr>
		<td class="infoa">Contact Email</td>
		<td><?php echo HTML::chars($ticket['contact_email']); ?></td>
	</tr>
	<?php if (!isset($akill)) { ?>
	<tr>
		<td class="infoa">Number of Hosts</td>
		<td><?php echo nl2br(HTML::chars($ticket['host_number'])); ?></td>
	</tr>	
	<?php } else { ?>
	<tr>
		<td class="infoa">Ban Message</td>
		<td><?php echo nl2br(HTML::chars($ticket['ban_message'])); ?></td>
	</tr>
	<?php } ?>
	<tr>
		<td class="infoa">Why</td>
		<td><?php echo nl2br(HTML::chars($ticket['why'])); ?></td>
	</tr>
	<tr>
		<td class="infoa">Added</td>
		<td><?php echo HTML::chars($ticket['added']); ?></td>
	</tr>
	<tr>
		<td class="infoa">Status</td>
		<td><?php echo Ticket::status($ticket['status']).($ticket['status'] == 1?' by '.HTML::chars($ticket['assignee_name']):''); ?></td>
	</tr>
</table>

<?php
if (Auth::instance()->getUser() !== null) {
?>
<p style="float: right"><a href="<?php echo URL::site('akills/view/'.$ticket['ticket'].'/n!'.sha1($ticket['ip'].$ticket['ticket'])); ?>">Share with non-kline team</a></p>
<?php
if (!isset($akill)) {

} else if ($akill === false) {
	echo '<p>Note: no matching akill record found under this IP.</p>';
} else {
?>
<h3>AKill information</h3>
<table class="info">
	<tr>
		<td class="infoa">Time</td>
		<td><?php echo HTML::chars(Time::relative($akill['time'])); ?></td>
	</tr>
	<tr>
		<td class="infoa">Message</td>
		<td><p><?php echo HTML::chars($akill['message']); ?></p></td>
	</tr>
	<tr>
		<td class="infoa">Added by</td>
		<td><?php echo HTML::chars($akill['addedby']); ?></td>
	</tr>
	<tr>
		<td class="infoa">Timestamp</td>
		<td><?php echo HTML::chars($akill['timestamp']); ?></td>
	</tr>
	<tr>
		<td class="infoa">AKill ID</td>
		<td><?php echo HTML::chars($akill['akillid']); ?></td>
	</tr>
</table>
<?php } ?>
<?php } ?>


<?php
if (Auth::instance()->getUser() !== null && $ticket['status'] != 3) {
?>
<h3>Assign ticket</h3>
<form action="<?php echo URL::site('akills/assign/'.$ticket['ticket']); ?>" method="post">
	<p>
			<label for="assignee">To</label>
			<?php
				$s = array('' => '(Unassign)', Auth::instance()->getUser() => Auth::instance()->getUserName());
				foreach ($staff->as_array() as $r) {
					if (Auth::instance()->getUser() != $r['user']) {
						$s[$r['user']] = $r['name'];
					}
				}
				unset($s[$ticket['assignee']]);
				echo Form::select('assignee', $s, $ticket['assignee']);
			?>
			<input type="submit" value="Assign">
	</p>
	<?php
		if ($ticket['status'] != 1) {
			echo '<p>(Note: Ticket status will change from '.Ticket::status($ticket['status']).' to '.Ticket::status(1).')</p>';
		}
	?>
</form>
<?php
}
?>

<h3>Replies</h3>
<?php
if (count($replies) > 0) {
?>	
<ul id="replies">
<?php
foreach($replies as $r) {
	echo '<li><p>';
	echo nl2br(Text::auto_link_urls(HTML::chars($r['message'])));
	echo '</p><strong>'.HTML::chars(($r['by_name']!=''?$r['by_name']:'Ticket Submitter ('.$r['ip'].')')).'</strong> at '.HTML::chars($r['at']);
	echo '</li>';
}
?>
</ul>
<?php
} else {
	echo '<p>No replies.</p>';
}
?>

<?php
if (!$noreply && (Auth::instance()->getUser() !== null || $ticket['status'] != 3)) {
?>
<h3 id="replyHeader">Reply to ticket</h3>
<form action="<?php echo URL::site('akills/reply/'.$ticket['ticket'].($hash!==null?'/'.$hash:'')); ?>" method="post">
	<ul>
		<li>
			<?php echo Form::textarea('message'); ?>
		</li>
<?php
/*if (Auth::instance()->getUser() !== null) {

		<li>
			<label for="status">New Ticket Status</label>
			<?php echo Form::select('status', array_slice(Ticket::$status, 1, null, true), $ticket['status']); ?>
		</li>

}*/
?>
	</ul>
<?php
switch ($ticket['status']) {
	case 2:
		if (Auth::instance()->getUser() !== null) {
			echo '<input type="submit" value="Re-open and assign to self" name="submit">';
			echo '<input type="submit" value="Keep resolved" name="submit">';
			echo '<input type="submit" value="Close" name="submit">';
		} else {
			echo '<input type="submit" value="Re-open" name="submit">';
			echo '<input type="submit" value="Close" name="submit">';
		}
		break;
	case 3:
		if (Auth::instance()->getUser() !== null) {
			echo '<input type="submit" value="Keep closed" name="submit">';
		}
		break;
	default:
		echo '<input type="submit" value="Reply" name="submit">';
		if (Auth::instance()->getUser() !== null) {
			echo '<input type="submit" value="Resolve" name="submit"> <input type="submit" value="Close" name="submit">';
		}
		break;
}
?>
</form>
<?php
}
?>

<?php
if (Auth::instance()->getUser() !== null) {
?>
<h3>Ticket history</h3>
<table id="history">
	<thead>
		<tr>
			<th id="historyTime">Time</th>
			<th id="historyUser">User</th>
			<th id="historyMessage">Message</th>
		</tr>
	</thead>
	<tbody>
	<?php 
	foreach ($history as $h) {
		echo '<tr><td>'.HTML::chars($h['at']).'</td><td>'.($h['by_name']!=''?HTML::chars($h['by_name']):'Ticket Submitter').'</td><td>'.HTML::chars($h['message']).'</td></tr>';
	}
	?>
	</tbody>
</table>
<?php
}
?>

<?php 
}
?>
