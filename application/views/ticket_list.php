<h2><?php echo $ticket_section_title; ?></h2>
<?php 
if (count($tickets) > 0) {
?>
<table class="tickets">
	<thead>
		<tr>
			<th>ID</th>
			<th>Type</th>
			<th>IP Address</th>
			<th>Contact Name</th>
			<th>Added</th>
			<th>Status</th>
			<th>Last Replier</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		foreach ($tickets as $t) {
			echo '<tr'.($t['status'] === '3' ? ' class="closed"':'').'><td><a href="'.URL::site('akills/view/'.$t['ticket']).'">#'.$t['ticket'].'</a></td><td>'.($t['host_number'] === '0' ? 'Ban' : 'SLI').'</td><td>'.$t['ip'].'</td><td>'.$t['contact_name'].'</td><td class="date">'.$t['added'].'</td><td>'.Ticket::status($t['status']).'</td><td>'.$t['last_replier'].'</td></tr>';
		}
		?>
	</tbody>
</table>
<?php 
} else if (isset($ticket_status_name)) {
?>
<p>No tickets <?php echo $ticket_status_name; ?>.</p>
<?php
}
?>

