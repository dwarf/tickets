<?php defined('SYSPATH') or die('No direct script access.');

// see http://kohanaframework.org/3.1/guide/database/config
return array(
	// local database for the ticket data
	'default' => array (
		'type' => 'mysql',
		'connection' => array(
			'hostname' => 'localhost',
			'username' => 'tickets',
			'password' => 'password123',
			'persistent' => FALSE,
			'database' => 'tickets',
		),
		'charset' => 'utf8',
		'table_prefix' => '',
	),
	// If you set this, we will attempt to fetch geo akills
	'geo' => array (
		'type' => 'mysql',
		'connection' => array(
			'hostname' => 'localhost',
			'username' => 'geouser',
			'password' => 'password123',
			'persistent' => FALSE,
			'database' => 'geo',
		),
		'charset' => 'utf8',
		'table_prefix' => '',
	),
	// Necessary in production setup; vBulletin database
	'vb' => array (
		'type' => 'mysql',
		'connection' => array(
			'hostname' => 'localhost',
			'username' => 'vb',
			'password' => 'password123',
			'persistent' => FALSE,
			'database' => 'vbulletin',
		),
		'charset' => 'utf8',
		'table_prefix' => '',
		// group ID of the user group to require for logging in
		'vb_group_id' => 10,
	),
);

